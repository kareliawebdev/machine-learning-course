// initialize object
const draw = {};

draw.path = (ctx, path, color = "black") => {

    ctx.strokeStyle    = color;
    ctx.lineWidth      = 3;
    ctx.beginPath();
    ctx.moveTo(...path[0]);

    for(let i=0; i<path.length;i++){

        ctx.lineTo(...path[i]);

    }

    // making line more roundish
    ctx.lineCap    = "round";
    ctx.lineJoin   = "round";

    ctx.stroke();

}

draw.paths = (ctx, paths, color = "black") => {

    for (const path of paths){
        draw.path(ctx, path, color);
    }
}

// for node code. Browser doesn't understand modules.
if(typeof module !== 'undefined'){
    module.exports=draw;
}