const utils={};

utils.flaggedUsers = [1663053145814];

utils.styles={
    car:{color: 'gray',text:'🚗'},
    fish:{color: 'red',text:'🐟'},
    house:{color: 'yellow',text:'🏠'},
    tree:{color: 'green',text:'🎄'},
    bicycle:{color: 'cyan',text:'🚳'},
    guitar:{color: 'blue',text:'🎸'},
    pencil:{color: 'mangenta',text:'✏'},
    clock:{color: 'lightgray',text:'🕚'},
}

utils.formatPercent = (number)=>{
    return (number * 100).toFixed(2) + "%"; // two decimals precicion
}

utils.printProgress = (count, max) => {

    // standard output, clear the line
    process.stdout.clearLine();

    // move cursor to beginning of line
    process.stdout.cursorTo(0);

    const percent = utils.formatPercent (
        count/max
    );

    process.stdout.write (count + "/" + max + " ("+percent+")");

}

/**
 * Array and key. Groups by key
 * @param {*} objArray 
 * @param {*} key 
 * @returns 
 */
utils.groupBy = (objArray, key) => {

    const groups = {};

    for (let obj of objArray) {

        const val = obj[key];
        if (groups[val] == null){
            groups[val] = [];
        }
        groups[val].push(obj);

    }
    return groups;
}

if (typeof module !=='undefined'){
    module.exports=utils;
}