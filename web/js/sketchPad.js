class SketchPad{

    constructor(container, size=400){
        this.canvas=document.createElement("canvas");
        this.canvas.width=size;
        this.canvas.height=size;
        this.canvas.style=`
            background-color: white;
            box-shadow: 0px 0px 10px 2px black;
        `;
        container.appendChild(this.canvas);

        const lineBreak = document.createElement("br");
        container.appendChild(lineBreak);

        this.undoBtn = document.createElement("button");
        this.undoBtn.innerHTML="UNDO";
        container.appendChild(this.undoBtn);


        this.ctx = this.canvas.getContext("2d");

        this.reset();

        // Event listener (private, cannot be called from outside)
        this.#addEventListeners();
    }

    reset(){
        this.paths      =[];
        this.isDrawing  = false;
        this.#redraw(); // redraw once to get undo-button to disabled.
    }

    #addEventListeners(){

        this.canvas.onmousedown = (evt) =>{

            const mouse = this.#getMouse(evt);

            this.paths.push([mouse]);
            this.isDrawing  = true;
            
        }

        this.canvas.onmousemove = (evt) =>{
            // only if drawing then do
            if (this.isDrawing){

                const mouse = this.#getMouse(evt);

                const lastPath=this.paths[this.paths.length-1];

                lastPath.push(mouse);
                this.#redraw();
            }
        }

        document.onmouseup = () =>{
            // when button is released, not drawing
            this.isDrawing=false;
            this.#redraw();
        }

        // for touchscreen users (like mobilephone)
        this.canvas.ontouchstart=(evt)=>{
            const loc=evt.touches[0];
            this.canvas.onmousedown(loc);
        }
        this.canvas.ontouchmove=(evt)=>{
            const loc=evt.touches[0];
            this.canvas.onmousemove(loc);
        }
        document.ontouchend=()=>{
            document.onmouseup();
        }

        // Undo-button event handler
        this.undoBtn.onclick=()=>{
            this.paths.pop();
            this.#redraw();
        }

    }

    // redraw method
    #redraw(){
        // start by clearing the canvas
        this.ctx.clearRect(0,0,
            this.canvas.width, this.canvas.height);
        console.log(this.paths);
        draw.paths(this.ctx, this.paths);

        // toggle undo button. If nothing to undo, disable
        if(this.paths.length>0){
            this.undoBtn.disabled=false;
        }else{
            this.undoBtn.disabled=true;
        }
    }

    // private method to get mouse postition
    #getMouse=(evt)=>{

        // rectange canvas bounding area
        const rect = this.canvas.getBoundingClientRect();

        // return mouse coordinates
        return [

            // x coordinate minus the left side of rectangle
            // coordinate relative to the left side of the canvas
            // and same to the y-coordinate
            Math.round(evt.clientX-rect.left),
            Math.round(evt.clientY-rect.top)

        ];
    }
}
    

